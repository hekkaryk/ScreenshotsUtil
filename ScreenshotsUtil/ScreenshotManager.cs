﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ScreenshotsUtil
{
    class ScreenshotManager
    {
        private static readonly string DesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        internal static void SaveSpecifiedArea(int width, int height, int sourceX, int sourceY, int destinationX, int destinationY, Size size, string path = null)
        {
            try
            {
                using (Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb))
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.CopyFromScreen(sourceX, sourceY, destinationX, destinationY, size, CopyPixelOperation.SourceCopy);
                    }

                    Clipboard.SetImage(bitmap);

                    if (path == null)
                    {
                        SaveFileDialog saveDialog = new SaveFileDialog
                        {
                            Filter = "PNG Image|*.png",
                            Title = "Save screenshot as"
                        };
                        saveDialog.ShowDialog();
                        if (saveDialog.FileName != string.Empty)
                            bitmap.Save(saveDialog.FileName, ImageFormat.Png);
                    }
                    else
                    {
                        bitmap.Save(path, ImageFormat.Png);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal static void SaveScreen(Screen screen, string path = null)
        {
            SaveSpecifiedArea(screen.Bounds.Width, screen.Bounds.Height, screen.Bounds.X, screen.Bounds.Y, 0, 0, screen.Bounds.Size, path);
        }

        internal static void SavePrimaryScreen()
        {
            SaveScreen(Screen.PrimaryScreen, GetFastDropPath);
        }

        internal static void SaveSelectedArea(Size currentSize, Point SourcePoint, Point DestinationPoint, Rectangle SelectionRectangle)
        {
            SaveSpecifiedArea(SelectionRectangle.Width, SelectionRectangle.Height, SourcePoint.X, SourcePoint.Y, DestinationPoint.X, DestinationPoint.Y, SelectionRectangle.Size, GetFastDropPath);
        }

        internal static void SaveAllScreens()
        {
            Directory.CreateDirectory("C:\\screenshots");
            Screen.AllScreens.ToList().ForEach(item => SaveScreen(item, $"C:\\screenshots\\{DateTime.Now.ToString().Replace(':', '-').Replace('/', '-')}.png"));
        }

        private static string GetFastDropPath
        {
            get
            {
                string path = Path.Combine(DesktopPath, $"{DateTime.Now.ToString().Replace(':', '-').Replace('/', '-')}.png");
                return path;
            }
        }
    }
}
