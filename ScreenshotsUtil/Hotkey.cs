﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ScreenshotsUtil;

namespace Hotkeys
{
    public class Hotkey
    {
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private int modifier;
        private int key;
        private IntPtr hWnd;
        private int id;

        public Hotkey(int modifier, Keys key, IntPtr formHandle)
        {
            this.modifier = modifier;
            this.key = (int)key;
            this.hWnd = formHandle;
            id = this.GetHashCode();

            if (!Register())
                MessageBox.Show("Hotkey failed to register!");
        }

        public bool Register()
        {
            return RegisterHotKey(hWnd, id, modifier, key);
        }

        public bool Unregiser()
        {
            return UnregisterHotKey(hWnd, id);
        }

        public override int GetHashCode()
        {
            return modifier ^ key ^ hWnd.ToInt32();
        }

        internal static void ProcessMessage(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                HandleHotkey(GetKey(m.LParam));
            }
        }

        private static Keys GetKey(IntPtr LParam)
        {
            return (Keys)((LParam.ToInt32()) >> 16);
        }

        private static void HandleHotkey(Keys key)
        {            
            ScreenshotManager.SavePrimaryScreen();
        }
    }
}
