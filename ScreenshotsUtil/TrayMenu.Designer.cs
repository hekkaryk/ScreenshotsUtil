﻿namespace ScreenshotsUtil
{
    partial class TrayMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrayMenu));
            this.mainMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.makeScreenshotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.secondaryScreensScreenshotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.addToStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeFromStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instantSavePrimaryScreenPrintScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instantSavePrimaryScreenPrintScreenToolStripMenuItem,
            this.selectAreaToolStripMenuItem,
            this.makeScreenshotToolStripMenuItem,
            this.secondaryScreensScreenshotsToolStripMenuItem,
            this.addToStartupToolStripMenuItem,
            this.removeFromStartupToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(292, 180);
            // 
            // makeScreenshotToolStripMenuItem
            // 
            this.makeScreenshotToolStripMenuItem.Name = "makeScreenshotToolStripMenuItem";
            this.makeScreenshotToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.makeScreenshotToolStripMenuItem.Text = "Save primary screen...";
            this.makeScreenshotToolStripMenuItem.Click += new System.EventHandler(this.TakeScreenshotInteractiveToolStripMenuItem_Click);
            // 
            // secondaryScreensScreenshotsToolStripMenuItem
            // 
            this.secondaryScreensScreenshotsToolStripMenuItem.Name = "secondaryScreensScreenshotsToolStripMenuItem";
            this.secondaryScreensScreenshotsToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.secondaryScreensScreenshotsToolStripMenuItem.Text = "Save all screens";
            this.secondaryScreensScreenshotsToolStripMenuItem.Click += new System.EventHandler(this.QuickSaveAllScreensToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.mainMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "ScreenshotsUtil";
            this.notifyIcon.Visible = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(284, 261);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // addToStartupToolStripMenuItem
            // 
            this.addToStartupToolStripMenuItem.Name = "addToStartupToolStripMenuItem";
            this.addToStartupToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.addToStartupToolStripMenuItem.Text = "Add to startup";
            this.addToStartupToolStripMenuItem.Click += new System.EventHandler(this.addToStartupToolStripMenuItem_Click);
            // 
            // removeFromStartupToolStripMenuItem
            // 
            this.removeFromStartupToolStripMenuItem.Name = "removeFromStartupToolStripMenuItem";
            this.removeFromStartupToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.removeFromStartupToolStripMenuItem.Text = "Remove from startup";
            this.removeFromStartupToolStripMenuItem.Click += new System.EventHandler(this.removeFromStartupToolStripMenuItem_Click);
            // 
            // selectAreaToolStripMenuItem
            // 
            this.selectAreaToolStripMenuItem.Name = "selectAreaToolStripMenuItem";
            this.selectAreaToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.selectAreaToolStripMenuItem.Text = "Screenshot selected area (Scroll Lock)";
            this.selectAreaToolStripMenuItem.Click += new System.EventHandler(this.selectAreaToolStripMenuItem_Click);
            // 
            // instantSavePrimaryScreenPrintScreenToolStripMenuItem
            // 
            this.instantSavePrimaryScreenPrintScreenToolStripMenuItem.Name = "instantSavePrimaryScreenPrintScreenToolStripMenuItem";
            this.instantSavePrimaryScreenPrintScreenToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.instantSavePrimaryScreenPrintScreenToolStripMenuItem.Text = "Instant save primary screen (Print Screen)";
            // 
            // TrayMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.richTextBox1);
            this.Name = "TrayMenu";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TrayMenu_FormClosing);
            this.Load += new System.EventHandler(this.TrayMenu_Load);
            this.mainMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem makeScreenshotToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem secondaryScreensScreenshotsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem instantSavePrimaryScreenPrintScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToStartupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeFromStartupToolStripMenuItem;
    }
}

