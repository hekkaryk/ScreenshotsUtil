﻿using System;
using System.IO;
using System.Windows.Forms;
using Hotkeys;

namespace ScreenshotsUtil
{
    public partial class TrayMenu : Form
    {
        private Hotkey MainHotkey { get; set; } = null;
        private KeyboardListener KeyboardListener { get; set; } = new KeyboardListener();
        string autostartPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\Hekkaryk's screenshot saver.exe";

        public TrayMenu()
        {
            InitializeComponent();
            MainHotkey = new Hotkey(Constants.NOMOD, Keys.PrintScreen, this.Handle);
            //KeyboardListener.OnKeyPressed += _listener_OnKeyPressed;
        }

        private void TrayMenu_Load( object sender, EventArgs e )
        {
            Hide();
            CheckStartUpStatus();
        }

        private void TrayMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MainHotkey.Unregiser())
                MessageBox.Show("Hotkey failed to unregister!");

            KeyboardListener.UnHookKeyboard();
        }

        internal static void CaptureArea()
        {
            CaptureAreaForm form = CaptureAreaForm.GetInstance();
            if (!form.Visible)
            {
                form.ShowDialog();
            }
            else
            {
                form.BringToFront();
            }
        }

        protected override void WndProc( ref Message m )
        {
            Hotkey.ProcessMessage(ref m);

            base.WndProc(ref m);
        }

        private void TakeScreenshotInteractiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScreenshotManager.SaveScreen(Screen.PrimaryScreen);
        }

        private void QuickSaveAllScreensToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScreenshotManager.SaveAllScreens();
        }

        private void ExitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Environment.Exit(0);
        }

        private void selectAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CaptureArea();
        }

        private void addToStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(autostartPath))
                File.Delete(autostartPath);
            File.Copy(Environment.CurrentDirectory + @"\ScreenshotsUtil.exe", autostartPath);
            CheckStartUpStatus();
        }

        private void removeFromStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(autostartPath))
                File.Delete(autostartPath);
            CheckStartUpStatus();
        }

        private void CheckStartUpStatus()
        {
            if (File.Exists(autostartPath))
            {
                addToStartupToolStripMenuItem.Visible = false;
                removeFromStartupToolStripMenuItem.Visible = true;
            }
            else
            {
                addToStartupToolStripMenuItem.Visible = true;
                removeFromStartupToolStripMenuItem.Visible = false;
            }
        }
    }
}
